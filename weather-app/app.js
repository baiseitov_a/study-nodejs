const request = require('request');
const geocode = require('./utils/geocode');

const url = 'https://api.darksky.net/forecast/44d9c6b38b2595ad1ac5d8194c02b385/37.8267,-122.4233';

// request({ url, json: true }, (error, response) => {
//   if(error){
//     console.log('Unable to connect to weather service!');
//   } else if(response.body.error) {
//     console.log('Unable to find location');
//   } else {
//     console.log(response.body.currently);
//   }
// });


geocode('Philadelphia', (error, data) => {
  console.log('Error', error);
  console.log('Data', data);
})